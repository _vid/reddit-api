//redditSource.js
/**
 * retrieve reddit posts for a specific sub-reddit.
 * Requirements:
  * feed base url [(string) http://www.reddit.com/]
  * feed source path: [(string) r/business]
  * numToShow [(int) 4]
  * filter [(string) new (new|rising)]
  * before [null] //first post
  * after [data.after] //last post
  * Functions:
   * initial load - document.ready
   * function formatData(post) - return the formated json data
   * function getAnother(afterBefore,name) - Load additional posts, before or after current list //ex: getAnother("after","t3_8ss8k6");

  * component
   * name
   * feed item title
   * num_comments
   * author
 */
//Initial vars
var subreddit = "business"; //string;
var subredditPrintable = "Business"; //string;
var filter = "new"; //string new|rising|hot
var limit = 4; //int
var initialLimit = limit;
var after = "";
var before = "";
var baseURL = "http://www.reddit.com";
var initialSource = baseURL + "/r/" + subreddit + "/" + filter + "/.json?jsonp=?&limit=" + initialLimit + "&after=" + after + "&before=" + before; //ex "http://www.reddit.com/r/business.json?jsonp=?"

/**
 * HEREDOC
 * Creating a JS method of HEREDOC, so we can have 'pretty' templates
 * Refs:
  * https://bitbucket.org/_vid/pup_req_job_ad_populate/src/86ad320800182ddd2549457dcd3fe973939bf1a5/pup_req_job_ad_populate.js?at=master&fileviewer=file-view-default
  * http://stackoverflow.com/questions/805107/creating-multiline-strings-in-javascript#comment-49227719
  * http://jsfiddle.net/orwellophile/hna15vLw/2/
 */
var HEREDOC;

function updateNav() {

  after = $('#reddit-content .post:last').attr('id');
  $(".nav#next").html('<a href="#" onclick="getAnother(\'after\',\'' + after + '\'); return false;" title="Load next post, if available"><span class="descriptive-text">Next</span><img src="images/downCircle.png" alt="Down arrow in a round circle" /></a>');
  before = $('#reddit-content .post:first').attr('id');
  $(".nav#prev").html('<a href="#" onclick="getAnother(\'before\',\'' + before + '\'); return false;" title="Click to load newer posts, if available"><span class="descriptive-text">Previous</span><img src="images/upCircle.png" alt="Up arrow in a round circle" /></a>');
}

function formatData(post) {
  plural = (post.data.num_comments === 1)? "": "s";
  id = post.data.name;
  url = post.data.url;
  title = post.data.title;
  permalink = post.data.permalink;
  num_comments = post.data.num_comments;
  author = post.data.author;
  //removing leading spaces in HEREDOC
  HEREDOC = function EOF() {/*!<<<EOF
<div class="post" id="${id}">
  <h2 class="title"><a href="${url}" title="Link directly to article: ${title}">${title}</a></h2>
  <hr class="divider" />
  <span class="author">Submitted by <a href="${baseURL}/u/${author}" title="Link to user page for ${author}">${author}</a></span>
  <span class="comments"><a href="${baseURL}${permalink}" title="Link to post on Reddit">${num_comments} comment${plural}</a></span>
</div>
EOF
*/};

  return HEREDOC.toString().split(HEREDOC.name)[2].replace(/\$\{([^}]+)\}/g, function(outer, inner, pos) {
    return this[inner];
  });
} //end function formatData

function getAnother(afterBefore, name) { //ex: getAnother("after","t3_8ss8k6");
  if (typeof(afterBefore) == null || typeof(name) == null) {
    //console.log('error missing var: afterBefore: [' + afterBefore + ']; name: [' + name + ']');
    return false;
  }
  limit = 1;
  before = "";
  after = "";

  switch (afterBefore) {
    case "after":
      after = name;
      break;
    case "before":
      before = name;
      break;
    default:
      after = name;
  }
  var source = baseURL + "/r/" + subreddit + "/" + filter + "/.json?jsonp=?&limit=" + limit + "&after=" + after + "&before=" + before;

  /*
    add one
    TODO: remove one
  */
  $.getJSON(source, function(data) {
    $.each(data.data.children, function(i, post) {
      if (afterBefore == "after") {
        $("#reddit-content").append(formatData(post)).fadeIn("slow");
        $('#reddit-content .post:first').fadeIn( "slow", function() {
          $(this).remove();
        });


      } else {
        $("#reddit-content").prepend(formatData(post)).fadeIn("slow");
        $('#reddit-content .post:last').fadeIn( "slow", function() {
          $(this).remove();
        });
      }
    });
    updateNav();
  });
} //end function getAnother(afterBefore,name)

//intially load limit-1 posts
$(document).ready(function() {
  $('#title').html('r/<span class="subreddit">' + subredditPrintable + '<span>'); //Set Title
  $('#reddit-content').html(''); //remove placeholder content
  $.getJSON(initialSource, function(data) {
    $.each(data.data.children, function(i, post) {
      $("#reddit-content").append(formatData(post));
    } //end function (i, post)
    ); //end each
    /*add nav*/
    $("#reddit-container").append('<div id="next" class="nav"></div>');
    $("#reddit-container").prepend('<div id="prev" class="nav"></div>');
    updateNav();
  });
}); //end $( document ).ready(function() initial load

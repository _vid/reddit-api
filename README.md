# README #

## Reddit-API ##

This was a 2 hour exercise, to pull posts from the r/business subreddit.

### Usage ###

Download the repository and launch index.html locally.

*Note*: This prototype is fully functional but due to CORS restrictions, it would need updates to function in a hosted environment.

### Branches ###

* The master branch contains what was done inside the 2 hour time limit. Essentially a prototype / proof of concept
* The dev branch contains tweaks; cleaning up and validating html, js and css; matching color values in Photoshop; nudging positioning and sizes; adding google font; adding proper box-shadow
* The dev-reddit-api-wrapper branch leverages an API Wrapper. This can be viewed on (codepen)[https://codepen.io/_vid/pen/NzBWKa]. The use of the API wrapper avoids the CORS issue that comes with making ajax calls to other domains. This branch also uses base64 encoded images and expanded debug functionality. Set debug = true in redditSource.js to see it in action.

### Goals ###

* Use PHP, or JavaScript, or a combination of the two for connecting to the API and outputting the results
* Do only what you can in 2 hours. If you don't finish or it's not as good as you'd like, send what you have anyway. Please let us know how much time you spent on the project and what else you would do if you had more time.
* Render the web page in CSS/HTML as pixel-perfect as possible to the original design
* Show 4 posts at a time and the arrows should scroll through the posts available
* Use good code design concepts and practices
* Make sure the page works and connects to the API properly

### Reference ###

* Example API call getting four results starting after a specific post: `https://www.reddit.com/r/business/new.json?after=t3_8sn4w1&limit=4`

### File Tree ###

```
├── flexbox-reset.css
├── images
│   ├── downCircle.png
│   ├── pw_maze_white.png
│   └── upCircle.png
├── index.html
├── redditSource.js
└── styles.css
```

### Highlights ###

* Any redundant code was abstracted into reusable functions. For example; the initial (and last) function which runs on page-load calls the [formatData()](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/redditSource.js#lines-51) and [updateNav()](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/redditSource.js#lines-43) functions, which are also called in the [getAnother()](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/redditSource.js#lines-75) function, i.e. whenever the previous and next navigation are used.
* CSS Reset. I included a modern CSS Reset, so while this was a quick and dirty effort, the initial cross-browser compatibility is good (Tested on the latest Safari, FF, and Chrome on MacOS).
* HEREDOC in JS with variable replacement:
<<<<<<< HEAD
 * Ex: [redditSource.js#lines-34](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/redditSource.js#lines-34) and [lines-59](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/redditSource.js#lines-59)
=======
    * Ex: [redditSource.js#lines-34](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/redditSource.js#lines-34) and [lines-59](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/redditSource.js#lines-59)
>>>>>>> dev
 ```
HEREDOC = function EOF() {/*!<<<EOF
<div class="post" id="${id}">
<h2 class="title"><a href="${url}" title="Link directly to article: ${title}">${title}</a></h2>
...
</div>
EOF
*/};
```
<<<<<<< HEAD
 * I've used this a number of times over the years and it's been a reliable way to bring the HEREDOC experience to JavaScript for prototypes and one-offs.
 * The js vars are wrapped with ${} and processed using a regex cooked up by mplungjan and Orwellophile a few years ago. [See stackoverflow for more](https://stackoverflow.com/questions/805107/creating-multiline-strings-in-javascript/14919898#14919898)
=======
    * I've used this a number of times over the years and it's been a reliable way to bring the HEREDOC experience to JavaScript for prototypes and one-offs.
    * The js vars are wrapped with ${} and processed using a regex cooked up by mplungjan and Orwellophile a few years ago. [See stackoverflow for more](https://stackoverflow.com/questions/805107/creating-multiline-strings-in-javascript/14919898#14919898)
>>>>>>> dev
* Quick customization to accommodate singular comment numbers: `plural = (post.data.num_comments === 1)? "": "s";` and used like so: `${num_comments} comment${plural}` which would output something like: `3 comments` or
`1 comment`

### Possible Next Steps ###

<<<<<<< HEAD
* Accessibility
* SCSS
* Cross-browser support
* Flexbox
* Responsive

* Accessibility
 * For this prototype, I ensured that links had title tags, images had alt tags and nav buttons had text (that was present for screen-readers but otherwise [swept off the screen](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/styles.css#lines-69). But the next steps would be adding ARIA tags for screen-readers, and validating with [wave](http://wave.webaim.org/) for WAI and WCAG compliance, and ARIA compatibility.
 * Recently I've been starting new projects with three priorities in mind: accessibility, off-line friendly, and mobile-first. All three of those were thrown out the window for this effort but their not hard to accommodate. Adding ARIA tags and sections like [landmarks](https://www.w3.org/TR/wai-aria-practices/examples/landmarks/HTML5.html) for screen-readers, requires little effort but has a big impact.
=======
#### Overview ####
* Accessibility
* Off-line friendly
* Cross-browser support
* Responsive

#### Details ####
* Accessibility
    * For this prototype, I did some of the standard accommodations, ensuring that links had title tags, images had alt tags and nav images had text that was present for screen-readers but otherwise [swept off the screen](https://bitbucket.org/_vid/reddit-api/src/post-2-hr-tweaks/styles.css#lines-69). The next steps would include adding ARIA tags for screen-readers, and validating with [wave](http://wave.webaim.org/) for WAI and WCAG compliance.
    * Adding ARIA tags and sections like [landmarks](https://www.w3.org/TR/wai-aria-practices/examples/landmarks/HTML5.html) for screen-readers, requires little effort but has a big impact.
* Off-line friendly
    * Ideally, all the API calls would be abstracted (like `getData(criteria ,callback)`) and fallback to a default dataset.
    * Abstracting the API calls allows us to keep clean code and easily change the methods as needed. In this case, since we were dealing with non-subscription data, my hard-coded ajax calls were directly to the standard json endpoints without implementing OAUTH or leveraging the available API Wrappers. Consequently, I've found this to be more reliable than some of the API Wrappers. But if we need to expand functionality or want to change methods (to deal with CORS issues for ex.) then we could change the methods behind the scenes.
 Adding a Wrapper like Reddit-API and replacing `$.getJSON()` with `reddit.new().fetch()` would be painless and the JSON result is the same for both calls.
    * For a default data set, we could store a sample json file and call that initially or in the case of loss connectivity, allowing the app. to be interactive off-line. Taking it a step further, we could include a database to hold the last data set.
* Cross-browser support
    * I started with a CSS reset, so the initial cross-browser style compatibility is good (Tested on the latest Safari, FF, and Chrome on MacOS).
    * I used the jQuery library which has good browser support. I also commented out console.log calls which can throw errors in some browsers.
    * Next steps: I would Cross-browser test the code on PC and add the obligatory IE meta tags for image quality and to favor Microsoft's more modern options when available.
    * I would update the CSS and JS as needed. For example, box-shadow and other modern CSS3 declarations require some tweaks for IE, Safari, etc...
    * I would leverage SCSS to clean up the style sheets and improve cross browser support
* Responsive
    * I would remove the fixed width, add breakpoints and leverage Flexbox for responsive layout.
    * I started with a Flexbox-reset but didn't leverage any flexbox features.

>>>>>>> dev

### Maintainer ###

* vid
